############################################
# Author: Daniel Flood <danielpflood@gmail.com>
# Date:   10.19.2015
############################################
#####  CREDITS  ############################
# Pool class comes almost exactly from http://www.burgestrand.se//code/ruby-thread-pool/
# instantmusic: https://github.com/yask123/Instant-Music-Downloader -> which relies on youtube-dl
############################################

#################### Configurable variables
@list_file="320_song_list.txt" #file that song names list will be stored in
@max_num_pages=1 #anything below 1 will cause all pages to be parsed
@max_num_downloads=20 #anything 0 or lower will download all songs
@instantmusic_flags="-q" # q is for quiet mode
@filesize_limit_mb="40" #currently not being used
@thread_pool_size=10 # adjust to your liking, number of concurrent downloads happening
############################################

########Check for gem dependencies##########
#begin
#  require 'optparse'
#rescue LoadError
#  puts "Required gem optparse not found. Installing now..."
#  `gem install optparse`
#  puts "done installing optparse"
#end

begin
  require 'nokogiri'
rescue LoadError
  puts "Required gem nokogiri not found. Installing now..."
  `gem install nokogiri`
  puts "done installing nokogiri"
end

begin
  require 'open-uri'
rescue LoadError
  puts "Required gem open-uri not found. Installing now..."
  `gem install open-uri`
  puts "done installing open-uri"
end

begin
  require 'thread'
rescue LoadError
  puts "Required gem thread not found. Installing now..."
  `gem install thread`
  puts "done installing thread"
end

#begin
#  require 'taco'
#rescue LoadError
#  puts "Required gem taco not found. Installing now..."
#  `gem install taco`
#  puts "done installing taco, now uninstalling because this was just a test"
#  `gem uninstall taco`
#  puts "done, sorry no more tacos"
#end
############################################


#TODO: Add command line options parsing
#options = {}
#OptionParser.new do |opts|
#  opts.banner = "Usage: 320_songs.rb [options]"
#
#  opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
#    options[:verbose] = v
#  end
#end.parse!
#
#p options
#p ARGV



class Pool
	def initialize(size)
		@size = size
		@jobs = Queue.new

		@pool = Array.new(@size) do |i|
			Thread.new do
				Thread.current[:id] = i
				catch(:exit) do
					loop do
						job, args = @jobs.pop
						job.call(*args)
					end
				end
			end
		end
	end

	def schedule(*args, &block)
		@jobs << [block, args]
	end

	def shutdown
		@size.times do 
			schedule { throw :exit }
		end
		@pool.map(&:join)
		puts "All threads shutdown."
	end
end

#########helper methods#####################
def start_downloading
	puts "Starting download of list of songs..."
	thread_arr = []
	count = 0
	dl_count = 0
	thread_pool = Pool.new(@thread_pool_size)
	File.open(@list_file) do |f|
		f.each_line do |line|
			cline=line.chomp
			if (@max_num_downloads > 0 && (dl_count >= @max_num_downloads))
				next
			else 
				dl_count+=1
				thread_pool.schedule do
					result = `instantmusic "#{@instantmusic_flags}" -s "#{cline}"`
					puts "#{cline} finished downloading by thread #{Thread.current[:id]}"
				end
			end
		end
	end
	at_exit { thread_pool.shutdown }
end

def load_page_songs(page_num)
	puts "Starting page #{page_num}..."
	page = Nokogiri::HTML(open("http://sweaty320.com/page/#{page_num}"))

	page.css("h2 a[rel='bookmark']").each do |song|
		File.open(@list_file, 'a') { |file| file.write(song.text+"\n") }
		puts "Added -> #{song.text}"
	end
end

def main_loop(generate_list)
	if generate_list 
		File.open(@list_file, 'w') { |file| file.write("") }

		page = Nokogiri::HTML(open("http://sweaty320.com/"))
		total_pages=page.css("span.pages").text.split.last.to_i
		if @max_num_pages>0 && total_pages>@max_num_pages
			puts "Limiting number of pages to retrieve to #{@max_num_pages}/#{total_pages} due to user configuration."
			total_pages=@max_num_pages
		end
		(1..total_pages).each do |pn|
			load_page_songs(pn)
		end
	end
	start_downloading
end

def command?(command)
   system("which #{ command} > /dev/null 2>&1")
end
############################################

############################ Program begins execution here #############################################

########Check for instantmusic#############
if not command?("instantmusic")
	fail("Missing instantmusic utility.\n Try running: \`pip install instantmusic\` to install the necessary python program.\n For additionl help go here: https://github.com/yask123/Instant-Music-Downloader")
end
############################################

if File.exist?(@list_file)
	print 'Found existing song list. Update this list? ([y]/n)'
	update_list = gets
	update_list = update_list.chomp
	if update_list == "y"
		main_loop(true)
	elsif update_list == "n"
		main_loop(false)
	elsif update_list == ""
		main_loop(true)
	else
		fail("invalid input, exiting")
	end
else
	main_loop(true)
end
############################################