############################################
# Author: Daniel Flood <danielpflood@gmail.com>
# Date:   10.19.2015
############################################
#####  CREDITS  ############################
# Pool class comes almost exactly from http://www.burgestrand.se//code/ruby-thread-pool/
# instantmusic: https://github.com/yask123/Instant-Music-Downloader -> which relies on youtube-dl
############################################

#################### Configurable variables
@list_file="320_song_list.txt" #file that song names list will be stored in
@max_num_pages=1 #anything below 1 will cause all pages to be parsed
@max_num_downloads=6 #anything 0 or lower will download all songs
@instantmusic_flags="-q" # q is for quiet mode
@filesize_limit_mb="40" #currently not being used
@thread_pool_size=6 # adjust to your liking, number of concurrent downloads happening
############################################