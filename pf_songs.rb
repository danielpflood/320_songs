#####################################################
# 		PitchFork Song Downloader 					#
#####################################################
# Author: Daniel Flood <danielpflood@gmail.com> 	#
# Initially Created: 	10.20.2015					#
# 			Updated: 	11.01.2015					#
#####################################################
# Platforms 										#
#   Made for Mac OS X El Capitan 					#
#####################################################


#####  CREDITS  ############################
# Pool class comes almost exactly from http://www.burgestrand.se//code/ruby-thread-pool/
# instantmusic: https://github.com/yask123/Instant-Music-Downloader -> which relies on youtube-dl
# terminal-notifier: 
############################################

#################### Configurable variables
@list_file="pf_tracks_list.txt" #file that song names list will be stored in
@start_page_num1
@total_pages_to_process=2
@max_num_downloads=0 #anything 0 or lower will download all songs
@instantmusic_flags="-q" # q is for quiet mode
@filesize_limit_mb="40" #currently not being used
@thread_pool_size=10 # adjust to your liking, number of concurrent downloads happening
@itunes_add_folder='/Users/danielpflood/Music/iTunes/iTunes Media/Automatically Add to iTunes.localized/'
@store_folder='/Users/danielpflood/Desktop/temp/'
@list_count = 0
############################################

# TODO:
# 	Look into  bundle package (http://bundler.io/v1.1/) for packaging
# 	Handle missing instant music/python
################################################

# Locating iTunes Automatic ADding Folder
=begin
Dir["**/"].each do |x| 
	puts x
end
=end


########Check for gem dependencies##########
#begin
#  require 'optparse'
#rescue LoadError
#  puts "Required gem optparse not found. Installing now..."
#  `gem install optparse`
#  puts "done installing optparse"
#end

begin
  require 'find'
rescue LoadError
  puts "Required gem find not found. Installing now..."
  `gem install find`
  puts "done installing find"
end

begin
  require 'fileutils'
rescue LoadError
  puts "Required gem fileutils not found. Installing now..."
  `gem install fileutils`
  puts "done installing fileutils"
end

begin
  require 'nokogiri'
rescue LoadError
  puts "Required gem nokogiri not found. Installing now..."
  `gem install nokogiri`
  puts "done installing nokogiri"
end

begin
  require 'open-uri'
rescue LoadError
  puts "Required gem open-uri not found. Installing now..."
  `gem install open-uri`
  puts "done installing open-uri"
end

begin
  require 'thread'
rescue LoadError
  puts "Required gem thread not found. Installing now..."
  `gem install thread`
  puts "done installing thread"
end

begin
  require 'terminal-notifier'
rescue LoadError
  puts "Desired gem terminal-notifier not found. Installing now..."
  `sudo gem install terminal-notifier`
  puts "done installing terminal-notifier"
end
############################################


#TODO: Add command line options parsing
#options = {}
#OptionParser.new do |opts|
#  opts.banner = "Usage: 320_songs.rb [options]"
#
#  opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
#    options[:verbose] = v
#  end
#end.parse!
#
#p options
#p ARGV



class Pool
	def initialize(size)
		@size = size
		@jobs = Queue.new

		@pool = Array.new(@size) do |i|
			Thread.new do
				Thread.current[:id] = i
				catch(:exit) do
					loop do
						job, args = @jobs.pop
						job.call(*args)
					end
				end
			end
		end
	end

	def schedule(*args, &block)
		@jobs << [block, args]
	end

	def shutdown
		@size.times do 
			schedule { throw :exit }
		end
		@pool.map(&:join)
		puts "All threads shutdown."

	end

	def queue_size
		return @jobs.length()
	end
end

#########helper methods#####################
def clean_str str, strip_parens=false
    # 'Big Boi Presents... Got Purp? Vol. 2' =>
    # 'Big Boi Presents Got Purp Vol. 2'
    str = str.gsub(/\.\.\./, '').gsub(/\?/, '')
    str = str.gsub(/'/,"")
    feat_index = str.downcase.index('feat.')
    if feat_index
      # 'Kryptonite - feat. Big Boi' => 'Kryptonite - '
      str = str[0...feat_index]
    end
    hyphen_index = str.index(' - ')
    if hyphen_index
      # 'Kryptonite - ' => 'Kryptonite'
      str = str[0...hyphen_index]
    end
    ellipsis_index = str.index('...')
    if strip_parens
      open_index = str.index('(')
      if open_index
        close_index = str.index(')', open_index)
        if close_index
          # 'Take Care (Explicit Deluxee)' => 'Take Care '
          str = str[0...open_index] + str[close_index+1...str.length]
        end
      end
    end
    # 'Take Care ' => 'Take Care'
    str.strip
  end

def move_files_to_folder
	puts "Moving all files to #{@store_folder}..."
	tc = 0

	Dir.glob(Dir.pwd + "/*.mp3").each do |f|
		#FileUtils.cp(f,@store_folder)
		File.rename(f, File.join(@itunes_add_folder, File.basename(f)))
		#FileUtils.mv f, @itunes_add_folder
		temp_name = File.basename(f)
		puts "\t > #{temp_name}"
		#clean_version = clean_str(temp_name)
		#puts "\t >> clean version: #{clean_version}"
		tc+=1
	end
	puts "Done moving files. \n All done :)"
#$ terminal-notifier -[message|group|list] [VALUE|ID|ID] [options]

	notification = `terminal-notifier -message "Finished!" -title "PF-Songs" -subtitle "\n#{tc} songs downloaded and moved." -group 11`
	#  `open #{@store_folder}`
	`open "#{@itunes_add_folder}"`
end

def start_downloading
	if @max_num_downloads==0
		puts "Downloading #{@list_count} songs (no limit set):"
	elsif @list_count>@max_num_downloads
		puts "Downloading #{@max_num_downloads} songs (due to maximum number of songs being set.)"
	else
		puts "Downloading #{@list_count} songs:"
	end
	puts "\t\tUsing #{@thread_pool_size} threads."
		
	thread_arr = []
	count = 0
	dl_count = 0
	thread_pool = Pool.new(@thread_pool_size)
	File.open(@list_file) do |f|
		f.each_line do |line|
			cline=line.chomp
			cline = cline.gsub('"',"")
			if (@max_num_downloads > 0 && (dl_count >= @max_num_downloads))
				next
			else 
				dl_count+=1
				thread_pool.schedule do
					result = `instantmusic "#{@instantmusic_flags}" -s "#{cline}"`
					qs=dl_count-thread_pool.queue_size+1
					puts "[#{qs}/#{dl_count}] >> #{cline} >> Thread:#{Thread.current[:id]} >> done."
				end
			end
		end
	end
	at_exit do
	 	thread_pool.shutdown 
		move_files_to_folder
	end
end

def load_page_songs(page_num)
	puts "Page #{page_num}."
	page = Nokogiri::HTML(open("http://pitchfork.com/reviews/tracks/#{page_num}"))
	songs = []
	page.css("li.player-target div.info h1 a").each do |link|
		artist = link.css("span.artist").text.strip
		title = link.css("span.title").text.strip
		artist.chomp! ":"
		song = "#{artist} - #{title}"
		File.open(@list_file, 'a') { |file| file.write("#{song}\n") }
		@list_count += 1
		puts "\t + #{song}"
	end
end

def main_loop(generate_list)
	if generate_list 
		File.open(@list_file, 'w') { |file| file.write("") }

		page = Nokogiri::HTML(open("http://pitchfork.com/reviews/tracks/"))
		end_num=@start_page_num+@total_pages_to_process-1
		(@start_page_num..end_num).each do |pn|
			load_page_songs(pn)
		end
	end
	start_downloading
end

def command?(command)
   system("which #{ command} > /dev/null 2>&1")
end
############################################

############################ Program begins execution here #############################################

########Check for instantmusic#############
if not command?("instantmusic")
	fail("Missing instantmusic utility.\n Try running: \`pip install instantmusic\` to install the necessary python program.\n For additionl help go here: https://github.com/yask123/Instant-Music-Downloader")
end
############################################

=begin
if File.exist?(@list_file)
	print 'Found existing song list. Update this list? ([y]/n)'
	update_list = gets
	update_list = update_list.chomp
	if update_list == "y"
		main_loop(true)
	elsif update_list == "n"
		main_loop(false)
	elsif update_list == ""
		main_loop(true)
	else
		fail("invalid input, exiting")
	end
else
	main_loop(true)
end
=end
main_loop(true)

############################################