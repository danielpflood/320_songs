!#/bin/bash
echo "Starting download 1..."
instantmusic -q -s "Piano Concerto no. 1 in E minor, Op. 11 - II. Romanze – Larghetto (String Quintet arr.)" &
echo "done with 1."
echo "Starting download 2..."
instantmusic -q -s "Piano Concerto no. 1 in E minor, Op. 11 - III. Rondo – Vivace (String Quintet arr.)" &
echo "done with 2."
echo "Starting download 3..."
instantmusic -q -s "Piano Concerto no. 1 in E minor, Op. 11 - I. Allegro Maestoso" &
echo "done with 3."
echo "Starting download 4..."
instantmusic -q -s "Piano Concerto no. 1 in E minor, Op. 11 - II. Romanze – Larghetto" &
echo "done with 4."
echo "Starting download 5..."
instantmusic -q -s "Piano Concerto no. 1 in E minor, Op. 11 - III. Rondo – Vivace" &
echo "done with 5."
echo "Done :)"